# Fungi DNA barcoding example

These data are from two ARISE test sites, Berkheide and Leidse Hout, where we soil samples were taken for DNA barcoding. 
  
The soil samples were processed through our DNA metabarcoding pipeline for fungi. 
The primers used target fungi, but not exclusively, some plants and animals will be detected as well, but these should be discarded (since the representation of these groups is likely biased by the primers).

We clustered the resulting sequences based on 97% similarity. This is an artificial way to approximate species. 
Thus each OTU is a 'species' of fungi and differs at least 3% from all other OTUs. 
  
In the attached sample sheet `ARISE_Sample information_logbook.xlsx` you can see which samples are from which site. 
The first tab has the conversion of the e-numbers to the sample numbers (e.g. BH = Berkheide). For the exact sampling of these samples, see the appropriate tab for that location (e.g. tab 3 Berkheide).

  The file `OTU97tab_tax.csv` contains the following information:

  - `OTUs`: an arbitrary number for each OTU detected in the study
  - `Samples`: the e-numbers are the sample numbers. For each OTUs it is indicated how many sequencing reads were detected in each sample. These numbers of reads are a very rough proxy for abundance. But alternatively, you can convert the numbers into presence absence, which is more conservative. 
  - The final columns are the identification of each OTUs based on reference datasets. Note that some OTUs are identified to species level, but many only get an identification to higher taxonomic level (this is due to the incompleteness of the databases).
  - The last column has the DNA sequence that represents each OTU.