#Radar data example

This folder contains sample of radar signatures and the features extracted from the signatures.

The data (signatures and features) is stored as an RDS file which you can import in R. The RDS files contains a list including to objects:

 - list of signatures
 - data frame with features
 
Here is a small R-script to read the data:


<code>
# READ ECHOES
ISIG <- readRDS(file='~/Downloads/ISig.RDS')
BSIG <- readRDS(file='~/Downloads/BSig.RDS')

# insects
insect_signatures <- ISIG[[1]] #insect signatures
insect_features <- ISIG[[2]] #insect features

# birds
bird_signatures <- BSIG[[1]] #bird signatures 
bird_features <- BSIG[[2]] #bird features

# example of signature plot
plot(bird_signatures[[1]], type='l' , main=names(bird_signatures)[1])
plot(insect_signatures[[1]], type='l' , main=names(insect_signatures)[1])
</code>