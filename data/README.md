### THESE DATA INCLUDE A MIXTURE OF PUBLISHED/UNPUBLISHED/SIMULATED DATA. ALL DATA IS FOR HACKATHON PURPOSES ONLY AND NOT FOR PUBLIC DISTRIBUTION

This directory contains 

#### Darwin Core Archive data 

from three sites:

* Limburg: <https://www.doi.org/10.15468/dl.v2v7p9>
* Veluwe: <https://www.doi.org/10.15468/dl.uzwbk6>
* Meijendel: <https://www.doi.org/10.15468/dl.ykxhcq>

DarwinCore is a standard promulgated by TDWG [as described here](https://dwc.tdwg.org/) and [in this paper](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0029715): . There is also an [R package](https://github.com/ropensci/finch)

#### Simulated camera trap records.
  Simulated of species identified from images taken by wildlife cameras. These data can be found in the `classified_image_data.csv` in the two of the site folders. 
  Simulations were used as we have no real classified images ready yet, though the final data structure will be very similar. 
  The code used to simulate these records based on the darwincore records can be found in the `gencamdata.R` R script.

  This file contains the following columns:

  - `deployment_id`: Unique identifier for the deployment of a sensor in a location for a certain period of time.
  - `x`: longitude of deployment
  - `y`: lattitude of deployment
  - `deployment_start`: When the deployment began
  - `device_type`: Type of sensor
  - `device_model`: Model of sensor
  - `site`: name of overall area of deployment
  - `location`: name of specific location of deployment
  - `species`: scientific name of species identified from image
  - `filename`: name of image from which species was identified

####Insect camera data

  Diopsis insect camera data for three sites. These data can be found in the `diopsis` folder in the the site folders. 

  Each site has 5 days of insect camera data. Each day has the a csv of the output classifications. 

  The output file contains the following columns:

 - `uid`: same as image ID
 - `filename`: <`uid`>.jpg
 - `fx1, x2, y1, y2`: relative box coordinates of the detected organism, added by detection
 - `fscore`: probability of the detected box (not the species!), not used, added by detection
 - `fsubset`: not used
 - `fclass_name`: not used
 - `flength_in_mm`: length in mm of the insect estimated from the predicted body segmentation, added by segmentation
 - `forder_0, order_0_probability`: predicted species and probability, note that taxa of any rank are in this column, the order name is for historic reasons, added by identification
 - `fobject_id`: cross-frame ID linking individual organisms across frames, added by tracking. Note the ID is only unique within the CSV, not across CSVs or cameras.
 - `fprevious_detection`: points to row number of linked detection, fragile, do not use, added by tracking
 - `ffirst_object_occurrence`: whether this is the first occurence of an individual organism, added by tracking
 - `fobject_order, object_order_probability`: predicted species and probability for individual organisms consisting of a combination of all detections belonging to individuals, note that taxa of any rank are in this column, the order name is for historic reasons, added by post-processing
 - `fbiomass`: estimated biomass in mg, added by biomass
 - `fbiomass_corrected`: estimated biomass in mg with some correction, added by biomass

#### Radar data

  Radar technology allows real-time detection of flying animals passing through its monitoring range, from small insects to large birds. 
  Machine learning algorithms use body mass, wing beat frequency and other detected features to classify obtained radar signals into different species groups (swifts, passerines, waders, bats, insects). 
  This allows researchers to investigate real-time, as well as long-term, animal movement patterns at a high temporal resolution.
  For instance, through radar it is possible to accurately quantify important behavioural processes such as bird and insect migration.
  
  See the Readme inside this folder for more info
  
#### Birdsong acoustic data
  The wetlands_bird_audio and individual_birds_audio folder each contains links to data relating to the automatic processing of acoustic birdsong data.
  
  See the Readme inside these folders for more info 
  
### DNA bardcoding data
  These data are from two ARISE test sites, where we soil samples were taken for DNA barcoding. 

  See the Readme inside this folder for more info


  
  
