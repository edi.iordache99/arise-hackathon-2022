
Datasets for automatic acoustic identification of individual birds
======================

The “automatic acoustic identification of individual birds” dataset contains sound recordings of 3 different bird species, and for each one the sound files are labelled with different individual bird identities. Can we automatically tell the difference between one owl and its neighbour? 

* Link to dataset: https://zenodo.org/record/1413495 -- you can download separately for each of the 3 species, littleowl-fg.zip, pipit-fg.zip, chiffchaff-fg.zip, or you can download the whole 9 GB data
* Paper: Stowell, D., Petrusková, T., Šálek, M., Linhart P. (2018) Automatic acoustic identification of individuals: Improving generalisation across species and recording conditions, Royal Society Interface 16(153) https://royalsocietypublishing.org/doi/10.1098/rsif.2018.0940



