# ARISE Hackathon 2022

Join us for the [ARISE Hackathon 2022][1]! ARISE is all about the diversity of
our natural species.  We’re losing them rapidly in the current biodiversity
crisis. But how do we stop this? Read more [about ARISE here][2].

In this year's hackathon we want to challenge you to blow our minds with
creative solutions that answer our main question:

> How do we make biodiversity impact visible? Or: what tools should be
> developed so any person can measure biodiversity impact or benefits?

## Data

See the [data](data/) directory for data sets that we have made available for
the hackathon.


[1]: https://www.arise-biodiversity.nl/hackathon
[2]: https://www.arise-biodiversity.nl/about
